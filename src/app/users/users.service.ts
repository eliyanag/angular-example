import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';
import { AngularFireDatabase } from 'angularfire2/database';
import { environment } from './../../environments/environment';


@Injectable()
export class UsersService {
  http:Http;
  getUsers(){
    //get messages from the SLIM rest API (Don't say DB)
    //return  this.http.get('http://localhost/slim/users');
    return  this.http.get(environment.url + 'users');
    }
    postUsers(data){
      //המרת גייסון למפתח וערך
      let options = {
      headers: new Headers({
      'content-type':'application/x-www-form-urlencoded'
      })
      }
      let params = new HttpParams().append('name',data.name).append('phone',data.phone);      
      //return this.http.post('http://localhost/slim/users', params.toString(), options);
      return this.http.post(environment.url + 'users', params.toString(), options);
    
      }
    deleteUser(key){
      //return this.http.delete('http://localhost/slim/users/'+ key);
      return this.http.delete(environment.url + 'users/'+ key);
    }
    getUser(key){  
     //return this.http.get('http://localhost/slim/users/'+key);
     return this.http.get(environment.url + 'users/'+key);
     }
              
     updateUser(data){       
     let options = {
     headers: new Headers({
      'content-type':'application/x-www-form-urlencoded'
     })   
    };
              
    let params = new HttpParams().append('name',data.name).append('phone',data.phone);  
     //return this.http.put('http://localhost/slim/users/'+ data.id,params.toString(), options);
     return this.http.put(environment.url + 'users/'+ data.id,params.toString(), options);        
    }

     //השרת שמתחבר לפיירבייס
    getUserFire(){
      //הוויליו מייצר את האובזווריבל
      return this.db.list('/users').valueChanges();
     }
  
    constructor(http:Http,private db:AngularFireDatabase) { 
      this.http = http;}

}