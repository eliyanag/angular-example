import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';

@Component({
  selector: 'users-firebase',
  templateUrl: './users-firebase.component.html',
  styleUrls: ['./users-firebase.component.css']
})
export class UsersFirebaseComponent implements OnInit {

  users;
  
  //only indepense injection
  constructor(private service:UsersService) { }

  //רוצים למשוך את הרשימה שהקומפוננט נוצר
  //פונקציה שמתעוררת ברגע שהקומפוננט נוצר לאחר הקונסטרקטור
  ngOnInit() {
    this.service.getUserFire().subscribe(response=>{
      console.log(response);
      this.users = response;
    });
  }

}

