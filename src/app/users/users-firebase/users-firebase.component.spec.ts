import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersFirebaseComponent } from './users-firebase.component';

describe('UsersFirebaseComponent', () => {
  let component: UsersFirebaseComponent;
  let fixture: ComponentFixture<UsersFirebaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersFirebaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersFirebaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
