// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url: 'http://localhost/slim/',
  firebase:{
    apiKey: "AIzaSyBpgG90P5OojzqjCUYqwq9zaSm-kw27f-Y",
    authDomain: "exam1-aab86.firebaseapp.com",
    databaseURL: "https://exam1-aab86.firebaseio.com",
    projectId: "exam1-aab86",
    storageBucket: "exam1-aab86.appspot.com",
    messagingSenderId: "999097399745"
  }
};
